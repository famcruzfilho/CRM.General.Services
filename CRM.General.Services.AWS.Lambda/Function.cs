using Amazon.Lambda.Core;
using CRM.General.Services.Crosscutting.IoC;
using CRM.General.Services.Domain.Contracts.Services;
using CRM.General.Services.Domain.Models;
using Microsoft.Extensions.DependencyInjection;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace CRM.General.Services.AWS.Lambda
{
	public class Function
    {
		private IUserService _userService;

		public Function()
		{
			_userService = DependencyResolver.GetContainer().GetService<IUserService>();
		}

		public void FunctionHandler(User user)
        {
			_userService.Create(user);
			var usr = _userService.GetNameOfUserById(user.UserId);
		}
	}
}