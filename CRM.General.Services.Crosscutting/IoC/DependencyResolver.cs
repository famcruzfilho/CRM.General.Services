﻿using CRM.General.Services.Business.Services;
using CRM.General.Services.Domain.Contracts.Repositories;
using CRM.General.Services.Domain.Contracts.Services;
using CRM.General.Services.Infraestructure.Data.Context;
using CRM.General.Services.Infraestructure.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace CRM.General.Services.Crosscutting.IoC
{
	public class DependencyResolver
	{
		//private static IServiceCollection _serviceCollection = new ServiceCollection();

		public static IServiceCollection _serviceCollection;
		public static ServiceProvider _serviceProvider;
		
		public static ServiceProvider GetContainer()
		{
			_serviceCollection = new ServiceCollection();

			//Context
			_serviceCollection.AddTransient<DataContext, DataContext>();

			//Services
			_serviceCollection.AddTransient<IUserService, UserService>();

			//Repositories
			_serviceCollection.AddTransient<IUserRepository, UserRepository>();

			_serviceProvider = _serviceCollection.BuildServiceProvider();

			return _serviceProvider;
		}
	}
}