﻿using CRM.General.Services.Domain.Contracts.Services.Base;
using CRM.General.Services.Domain.Models;

namespace CRM.General.Services.Domain.Contracts.Services
{
	public interface IUserService : IBaseService<User>
	{
		User GetNameOfUserById(int id);
	}
}