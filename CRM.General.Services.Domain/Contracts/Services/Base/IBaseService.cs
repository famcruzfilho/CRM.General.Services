﻿namespace CRM.General.Services.Domain.Contracts.Services.Base
{
	public interface IBaseService<TEntity> where TEntity : class
	{
		void Create(TEntity obj);
		TEntity GetById(int id);
		void Update(TEntity obj);
		void Remove(TEntity obj);
	}
}