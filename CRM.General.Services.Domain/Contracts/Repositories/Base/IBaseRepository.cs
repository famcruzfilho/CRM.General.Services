﻿namespace CRM.General.Services.Domain.Contracts.Repositories.Base
{
	public interface IBaseRepository<TEntity> where TEntity : class
	{
		void Create(TEntity obj);
		TEntity GetById(int id);
		void Update(TEntity obj);
		void Remove(TEntity obj);
	}
}