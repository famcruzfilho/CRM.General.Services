﻿using Amazon.SQS.Model;
using System.Threading.Tasks;

namespace CRM.General.Services.Domain.Contracts.Repositories.Base
{
	public interface IBaseSQSRepository<TEntity> where TEntity : class
	{
		Task<ReceiveMessageResponse> GetAll();
		void RemoveByRecieptHandle(string recieptHandle);
		void Create(TEntity obj);
	}
}