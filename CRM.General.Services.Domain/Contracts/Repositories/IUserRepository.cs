﻿using CRM.General.Services.Domain.Contracts.Repositories.Base;
using CRM.General.Services.Domain.Models;

namespace CRM.General.Services.Domain.Contracts.Repositories
{
	public interface IUserRepository : IBaseRepository<User>
	{
		User GetNameOfUserById(int id);
	}
}