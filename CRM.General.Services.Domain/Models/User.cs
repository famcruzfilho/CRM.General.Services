﻿namespace CRM.General.Services.Domain.Models
{
	public class User
	{
		public int UserId { get; set; }
		public string Name { get; set; }
	}
}