﻿using System;
using System.Runtime.Serialization;

namespace CRM.General.Services.Domain.Models.AWS
{
	[Serializable]
	public class AWS : ISerializable
	{
		public string ServiceURL { get; set; }
		public string AccessKeyId { get; set; }
		public string SecretAccessKey { get; set; }
		public string QueueURL { get; set; }
		public string RecieptHandle { get; set; }

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("ServiceURL", ServiceURL);
			info.AddValue("AccessKeyId", AccessKeyId);
			info.AddValue("SecretAccessKey", SecretAccessKey);
			info.AddValue("QueueURL", QueueURL);
			info.AddValue("RecieptHandle", RecieptHandle);
		}
	}
}