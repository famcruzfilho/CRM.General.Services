﻿namespace CRM.General.Services.Domain.DTOs
{
	public class UserDTO
	{
		public int UserId { get; set; }
		public string Name { get; set; }
	}
}