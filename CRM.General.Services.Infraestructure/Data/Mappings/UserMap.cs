﻿using CRM.General.Services.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CRM.General.Services.Infraestructure.Data.Mappings
{
	public class UserMap : IEntityTypeConfiguration<User>
	{
		public void Configure(EntityTypeBuilder<User> builder)
		{
			//builder.ToTable("Users");

			builder.HasKey(x => x.UserId);

			builder.Property(x => x.Name)
				.IsRequired()
				.HasMaxLength(100);
		}
	}
}