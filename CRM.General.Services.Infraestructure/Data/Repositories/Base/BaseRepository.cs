﻿using CRM.General.Services.Domain.Contracts.Repositories.Base;
using CRM.General.Services.Infraestructure.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace CRM.General.Services.Infraestructure.Data.Repositories.Base
{
	public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
	{
		private readonly DataContext _dataContext;

		public BaseRepository(DataContext dataContext)
		{
			_dataContext = dataContext;
		}

		public void Create(TEntity obj)
		{
			_dataContext.Set<TEntity>().Add(obj);
			_dataContext.SaveChanges();
		}

		public void Remove(TEntity obj)
		{
			_dataContext.Set<TEntity>().Remove(obj);
			_dataContext.SaveChanges();
		}

		public TEntity GetById(int id)
		{
			return _dataContext.Set<TEntity>().Find(id);
		}

		public void Update(TEntity obj)
		{
			_dataContext.Set<TEntity>().Attach(obj);
			_dataContext.Entry(obj).State = EntityState.Modified;
			_dataContext.SaveChanges();
		}
	}
}