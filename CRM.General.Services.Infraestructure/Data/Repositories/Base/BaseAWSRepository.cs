﻿using Amazon.SQS.Model;
using CRM.General.Services.Domain.Contracts.Repositories.Base;
using CRM.General.Services.Infraestructure.Data.Context;
using System.Threading.Tasks;

namespace CRM.General.Services.Infraestructure.Data.Repositories.Base
{
	class BaseAWSRepository<TEntity> : IBaseSQSRepository<TEntity> where TEntity : class
	{
		private readonly AWSContext<TEntity> _AWSContext;

		public BaseAWSRepository(AWSContext<TEntity> AWSContext)
		{
			_AWSContext = AWSContext;
		}

		public async Task<ReceiveMessageResponse> GetAll()
		{
			return await _AWSContext.GetAll();
		}

		public void RemoveByRecieptHandle(string recieptHandle)
		{
			_AWSContext.RemoveByRecieptHandle(recieptHandle);
		}

		public void Create(TEntity obj)
		{
			_AWSContext.Create(obj);
		}
	}
}