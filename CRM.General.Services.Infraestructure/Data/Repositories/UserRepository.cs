﻿using CRM.General.Services.Domain.Contracts.Repositories;
using CRM.General.Services.Domain.Models;
using CRM.General.Services.Infraestructure.Data.Context;
using CRM.General.Services.Infraestructure.Data.Repositories.Base;
using System.Linq;

namespace CRM.General.Services.Infraestructure.Data.Repositories
{
	public class UserRepository : BaseRepository<User>, IUserRepository
	{
		private readonly DataContext _dataContext;

		public UserRepository(DataContext dataContext) : base(dataContext)
		{
			_dataContext = dataContext;
		}

		public User GetNameOfUserById(int id)
		{
			return _dataContext.Users.FirstOrDefault(x => x.UserId == id);
		}
	}
}