﻿using CRM.General.Services.Domain.Models;
using CRM.General.Services.Infraestructure.Data.Mappings;
using Microsoft.EntityFrameworkCore;

namespace CRM.General.Services.Infraestructure.Data.Context
{
	public class DataContext : DbContext
	{
		//https://blog.tonysneed.com/2018/12/21/use-ef-core-with-aws-lambda-functions/

		public DataContext()
		{
		}

		public DataContext(DbContextOptions<DataContext> options) : base(options)
		{
		}

		public DbSet<User> Users { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.ApplyConfiguration(new UserMap());
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlServer("Server=localhost\\SQLEXPRESS;Database=CRMGeneralServices;User Id=sa;password=Em0t10n%;Trusted_Connection=False;MultipleActiveResultSets=true;");
		}
	}
}