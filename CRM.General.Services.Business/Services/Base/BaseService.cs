﻿using CRM.General.Services.Domain.Contracts.Repositories.Base;
using CRM.General.Services.Domain.Contracts.Services.Base;

namespace CRM.General.Services.Business.Services.Base
{
	public class BaseService<TEntity> : IBaseService<TEntity> where TEntity : class
	{
		private readonly IBaseRepository<TEntity> _repository;

		public BaseService(IBaseRepository<TEntity> repository)
		{
			_repository = repository;
		}

		public void Create(TEntity obj)
		{
			_repository.Create(obj);
		}

		public void Remove(TEntity obj)
		{
			_repository.Remove(obj);
		}

		public TEntity GetById(int id)
		{
			return _repository.GetById(id);
		}

		public void Update(TEntity obj)
		{
			_repository.Update(obj);
		}
	}
}