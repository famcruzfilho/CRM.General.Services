﻿using Amazon.SQS.Model;
using CRM.General.Services.Domain.Contracts.Repositories.Base;
using CRM.General.Services.Domain.Contracts.Services.Base;
using System.Threading.Tasks;

namespace CRM.General.Services.Business.Services.Base
{
	public class BaseSQSService<TEntity> : IBaseSQSService<TEntity> where TEntity : class
	{
		private readonly IBaseSQSRepository<TEntity> _baseSQSRepository;

		public BaseSQSService(IBaseSQSRepository<TEntity> baseSQSRepository)
		{
			_baseSQSRepository = baseSQSRepository;
		}

		public void Create(TEntity obj)
		{
			_baseSQSRepository.Create(obj);
		}

		public Task<ReceiveMessageResponse> GetAll()
		{
			return _baseSQSRepository.GetAll();
		}

		public void RemoveByRecieptHandle(string recieptHandle)
		{
			_baseSQSRepository.RemoveByRecieptHandle(recieptHandle);
		}
	}
}