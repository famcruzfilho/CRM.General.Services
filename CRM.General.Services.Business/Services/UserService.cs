﻿using CRM.General.Services.Business.Services.Base;
using CRM.General.Services.Domain.Contracts.Repositories;
using CRM.General.Services.Domain.Contracts.Services;
using CRM.General.Services.Domain.Models;

namespace CRM.General.Services.Business.Services
{
	public class UserService : BaseService<User>, IUserService
	{
		private readonly IUserRepository _userRepository;

		public UserService(IUserRepository userRepository) : base(userRepository)
		{
			_userRepository = userRepository;
		}

		public User GetNameOfUserById(int id)
		{
			return _userRepository.GetNameOfUserById(id);
		}
	}
}